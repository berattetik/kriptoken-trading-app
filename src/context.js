import React, { Component } from 'react'

export const Context = React.createContext();

const reducer = (state, action) => {
    switch (action.type) {
        case "GET_CURRENCIES":
            return {
                ...state,
                currencies: null
            }
        case "SAVE_TOKEN":
            return {
                ...state,
                token: action.payload.token,
                email: action.payload.email
            }
        default:
            return state
    }
}

export class Provider extends Component {
    state = {
        name: "",
        email: "",
        password: "",
        token: null,
        currencies: null,
        navbar: {
            title: "Default App"
        },
        dispatch: action => {
            this.setState(state => reducer(state, action))
        }
    };
    updateState(value) {
        this.setState({ value });
    }
    render() {
        const { children } = this.props;
        return (
            <Context.Provider value={this.state} >
                {children}
            </Context.Provider >
        )
    }
}

const Consumer = Context.Consumer;

export default Consumer;