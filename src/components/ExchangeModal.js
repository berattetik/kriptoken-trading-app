import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    FormGroup,
    Label,
    Input,
    Col,
    Container,
    Row,
    InputGroup,
    InputGroupAddon
} from 'reactstrap';
import Cookies from 'universal-cookie';

const ExchangeModal = (props) => {
    const {
        isOpen,
        openModal,
        closeModal,
        type,
        exchange,
        exchangeType,
        currencies
    } = props;

    useEffect(() => prepareState(), []);

    const [values, setValues] = useState({ amount: "", coinType: '', exType: '' })

    const prepareState = () => {
        setValues({ amount: "", exType: currencies.filter((currency) => { return currency.name !== type ? currency : null })[0].name });
    }

    const setTokenAmount = (e) => {
        e.persist();
        setValues(prevState => ({ ...prevState, amount: e.target.value }))
    }

    const setExchangeType = (e) => {
        e.persist()
        setValues(prevState => {
            return { ...prevState, exType: e.target.value }
        })
    }

    const close = () => {
        closeModal();
        prepareState();
    }

    const submit = () => {
        const buyOrSell = exchangeType === "buy" ? "" : "-"
        let url = `http://localhost:3001/api/v1/trading/${values.exType.toLowerCase()}?amount=${buyOrSell}${values.amount}&buy=${type.toLowerCase()}`
        const cookies = new Cookies();
        let token = cookies.get('token');
        let config = {
            headers: { 'Authorization': "Bearer " + token }
        };
        axios
            .post(url, {}, config)
            .then(result => {
                exchange(values);
                close();
            })
            .catch(err => {
                console.log(err);
            })

        prepareState();
    }

    return (
        <div >
            <Modal isOpen={isOpen} toggle={openModal} className="modal-lg" style={{ marginTop: "calc(25%)" }}>
                <ModalHeader>{`${exchangeType ? exchangeType.toUpperCase() : null} ${type ? type.toUpperCase() : null}`} </ModalHeader>
                <ModalBody>
                    <Container>
                        <Row xs="1" md="2" >
                            <Col>
                                <FormGroup row>
                                    <Col sm={10}>
                                        <InputGroup>
                                            <Input
                                                placeholder="0,00"
                                                min={0}
                                                max={10000}
                                                type="number"
                                                onChange={setTokenAmount}
                                                value={values.amount}
                                                step="0.01"
                                                required
                                            />
                                            <InputGroupAddon addonType="append">{type}</InputGroupAddon>
                                        </InputGroup>
                                    </Col>
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup row>
                                    <Label for="exampleSelect" sm={2}> {exchangeType == "buy" ? "from" : "to"}</Label>
                                    <Col sm={10}>
                                        <Input
                                            type="select"
                                            name="select"
                                            id="tokenSelect"
                                            value={values.exType}
                                            onChange={setExchangeType}>
                                            {currencies.map(({ name, value }) => {
                                                return name !== type ? <option key={name}>{name}</option> : null
                                            })}
                                        </Input>
                                    </Col>
                                </FormGroup>
                            </Col>
                        </Row>
                    </Container>

                </ModalBody>
                <ModalFooter>
                    <Button
                        color="primary"
                        disabled={values.amount && values.amount !== "" && values.amount !== "0" ? false : true}
                        onClick={submit}>
                        {exchangeType ? (exchangeType[0].toUpperCase() + exchangeType.slice(1)) : "Submit"}
                    </Button>
                    <Button color="secondary" onClick={close}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default ExchangeModal;