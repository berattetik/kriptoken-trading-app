import React, { useState, useEffect } from 'react';
import {
    Collapse,
    Navbar,
    NavbarBrand,
    Nav,
    Button
} from 'reactstrap';
import Cookies from 'universal-cookie';
import axios from 'axios';

const NavigationHeader = (props) => {
    const [cookie, setCookie] = useState(false);

    useEffect(() => setCookie((new Cookies()).get('token')), []);

    const signOut = () => {
        document.cookie = 'token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        setCookie(false);
        window.location.replace('/');
        // axios
        //     .post('http://localhost:3001/api/v1/logout', {})
        //     .then(response => {
        //         setCookie(false);
        //         if (response.redirected) {
        //             return window.location.replace(response.url);
        //         }
        //     }).catch(err => {
        //         console.log("err", err)
        //     });
    }

    return (
        <div>
            <Navbar light expand="md" className="shadow bg-white rounded fixed-top"  >
                <NavbarBrand href="/">Kriptoken Trading Platform</NavbarBrand>
                <Collapse navbar>
                    <Nav className="mr-auto" navbar>
                    </Nav>
                    {cookie ? <Button className="btn-outline-danger" color="transparent" type="button" onClick={signOut}>
                        Sign out
                            <i className="far fa-sign-out-alt ml-1"></i>
                    </Button> : ""
                    }
                </Collapse>
            </Navbar>
        </div>
    );
}

export default NavigationHeader;