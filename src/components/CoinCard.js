import React, { useEffect, useState } from 'react';
import { Card, Button, ButtonGroup, CardTitle, CardText, Col, Container, } from 'reactstrap';
import Consumer from '../context';
import ExchangeModal from './ExchangeModal';

// class Users extends Component {
const CoinCard = (props) => {
    const { type, updateValues } = props;
    const [icon, setIcon] = useState("")
    const [isModalOpen, toggle] = useState(false)
    const [exchangeType, setExchangeType] = useState(null)

    useEffect(() => setIconClass(), []);

    const setIconClass = () => {
        let iconClass = "";
        switch (type) {
            case "LTC":
                iconClass = "fas fa-lira-sign"
                break;
            case "ETH":
                iconClass = "fab fa-ethereum"
                break;
            case "DASH":
                iconClass = "fab fa-dyalog"
                break;
            case "XRP":
                iconClass = "fab fa-btc"
                break;
        }
        setIcon(iconClass)
    }

    const closeModal = () => {
        toggle(false);
    }

    const buy = (e) => {
        setExchangeType("buy")
        toggle(!isModalOpen);
    }

    const sell = (e) => {
        setExchangeType("sell")
        toggle(!isModalOpen);
    }

    const exchange = (values) => {
        updateValues(values);
    }

    return (<Consumer>
        {
            value => {
                const { dispatch } = value;
                const { amount } = props
                return (
                    <Col className="mt-4">
                        <ExchangeModal
                            isOpen={isModalOpen}
                            openModal={buy}
                            closeModal={closeModal}
                            exchange={exchange}
                            type={type}
                            exchangeType={exchangeType}
                            currencies={props.currencies}
                        />
                        <Container>
                            <Card body>
                                <CardTitle>
                                    <i className={`${icon} mr-2`}></i>
                                    {type}</CardTitle>
                                <CardText className="text-center">{amount}</CardText>
                                <ButtonGroup>
                                    <Button color="white" className="rounded mr-4 btn btn-outline-primary" onClick={buy}>Buy</Button>
                                    <Button color="white" className="rounded btn btn-outline-primary" onClick={sell}>Sell</Button>
                                </ButtonGroup>
                            </Card>
                        </Container>
                    </Col>
                );
            }
        }
    </Consumer>)
};

export default CoinCard;