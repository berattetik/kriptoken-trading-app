import React, { useState, useEffect } from 'react';
import { Row, Col, Container, Table } from 'reactstrap';

const Currency = (props) => {
    const [currencies, setCurrencies] = useState(null)

    useEffect(() => setCurrencies(props.currencies), []);

    return (
        <Row xs="1" sm="2" md="2">
            <Col className="mt-4">
                <Container>
                    <Table responsive hover bordered>
                        <thead>
                            <tr>
                                <th>Currency</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        {currencies &&
                            currencies.map(({ name, value }) => {
                                return (
                                    <tbody key={name}>
                                        <tr>
                                            <td>1 {name}</td>
                                            <td>{value}</td>
                                        </tr>
                                    </tbody>
                                )
                            })
                        }
                    </Table>
                </Container>
            </Col>
        </Row>
    );
};

export default Currency;