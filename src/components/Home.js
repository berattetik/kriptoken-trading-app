import React, { useState, useEffect } from 'react';
import Consumer, { Context } from '../context';
import CoinCardList from './CoinCardList';
import Currency from './Currency';
import axios from 'axios';
import Cookies from 'universal-cookie';


const Home = () => {
    // const context = useContext(Context);
    const [currencies, setCurrencies] = useState(null)
    const [userBalance, setUserBalance] = useState(null)

    useEffect(() => getCurrencies(), []);

    const getCurrencies = () => {
        const cookies = new Cookies();

        if (cookies.get('token'))
            axios
                .get('http://localhost:3001/api/v1/trading/getAllExchange',
                    {
                        headers: { 'Authorization': "Bearer " + cookies.get('token') }
                    })
                .then((result) => {
                    setCurrencies(result.data.currencies)
                    setUserBalance(result.data.user.balance)
                    // dispatch({ type: "GET_CURRENCIES", payload: result.data.currencies })
                }).catch((err) => {
                    //show error dialog message
                    console.log(err)
                });
    }

    return (<Consumer>
        {
            () => {
                if (!(new Cookies()).get('token'))
                    window.location.replace('/');
                return (
                    <div className="mt-5">
                        {userBalance && <CoinCardList updateValues={getCurrencies} balance={userBalance} currencies={currencies} />}
                        {currencies && <Currency currencies={currencies} />}
                    </div>
                )
            }
        }
    </Consumer>
    )
}
export default Home;