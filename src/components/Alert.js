import React from 'react';
import { UncontrolledAlert } from 'reactstrap';

const AlertBox = (props) => {

  return (
    <div className="mt-2">
      {/* <Alert color="primary">
        This is a primary alert with <a href="#" className="alert-link">an Alert link</a>. Give it a click if you like.
      </Alert>
      <Alert color="secondary">
        This is a secondary alert with <a href="#" className="alert-link">an Alert link</a>. Give it a click if you like.
      </Alert>
      <Alert color="success">
        This is a success alert with <a href="#" className="alert-link">an Alert link</a>. Give it a click if you like.
      </Alert> */}
      <UncontrolledAlert color={props.type} fade={true}>
        {/* You entered invalid <b>{props.error}</b>. Please enter valid e-mail address */}
        {props.isDummyErr ? "Something went wrong!" : ""}  <b>{props.error}</b>
        {/* This is a danger alert with <a href="#" className="alert-link">an Alert link</a>. Give it a click if you like. */}
      </UncontrolledAlert>
      {/* <Alert color="warning">
        This is a warning alert with <a href="#" className="alert-link">an Alert link</a>. Give it a click if you like.
      </Alert>
      <Alert color="info">
        This is a info alert with <a href="#" className="alert-link">an Alert link</a>. Give it a click if you like.
      </Alert>
      <Alert color="light">
        This is a light alert with <a href="#" className="alert-link">an Alert link</a>. Give it a click if you like.
      </Alert>
      <Alert color="dark">
        This is a dark alert with <a href="#" className="alert-link">an Alert link</a>. Give it a click if you like.
      </Alert> */}
    </div>
  );
};

AlertBox.defaultProps = {
  isDummyErr: true,
  type: "danger"
};

export default AlertBox;