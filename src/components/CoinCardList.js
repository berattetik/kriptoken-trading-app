import React from 'react';
import CoinCard from './CoinCard'
import { Row } from 'reactstrap';
// class Users extends Component {
const CoinCardList = (props) => {
    const { balance, currencies, updateValues } = props;

    return (
        <Row xs="1" sm="2" md="4">
            {
                balance ?
                    Object.entries(balance).map(coin => {
                        return <CoinCard updateValues={updateValues} currencies={currencies} key={coin[0]} amount={Object.values(coin[1])[0]} type={coin[0]} />
                    }) : null
            }
        </Row>
    )
};

export default CoinCardList;