import React, { Component } from 'react'
import {
    FormGroup, Label, Input
} from 'reactstrap';
import validator from 'validator';

export default class FormInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            [this.props.type]: ""
        }
    }

    valueUpdate = (e) => {
        const { isSignup, name, email, password } = this.state;
        this.setState({ [e.target.name]: e.target.value })
        if (isSignup)
            this.setState({ isValid: !(name && email && validator.isEmail(email) && password) })
        else
            this.setState({ isValid: !(email && validator.isEmail(email) && password) })
    }
    render() {
        const { type } = this.props;
        const { name } = this.state;
        return (
            <FormGroup>
                <Label htmlFor={type}>FullName</Label>
                <Input
                    type={type}
                    name={type}
                    id={type}
                    placeholder="John Doe"
                    value={this.state[type]}
                    // disabled={disabledInput}
                    onChange={this.valueUpdate.bind(this, dispatch)}
                />
                {/* {errors.name ? <AlertBox type="danger" error="Name" /> : ""} */}
            </FormGroup>
        )
    }
}
