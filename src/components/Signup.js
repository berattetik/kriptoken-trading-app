import React, { Component } from 'react'
import {
    Container, Col, Form,
    FormGroup, Label, Input,
    Button, Card, CardTitle, Row
} from 'reactstrap';
import AlertBox from './Alert'
import validator from 'validator';
import axios from 'axios';
import Consumer from '../context';
// import FormInput from './FormInput';
import Cookies from 'universal-cookie';

export default class Signup extends Component {
    state = {
        isSignup: false,
        isForgotPassword: false,
        isValid: true,
        email: "",
        name: "",
        password: "",
        disabledInput: false,
        success: {
            general: false,
        },
        errors: {
            name: false,
            email: false,
            password: false,
            general: false
        }
    }
    cookies = (param) => {
        return (new Cookies()).get(param)
    }

    registerOrGoBack = () => {
        if (this.state.isForgotPassword) {
            this.setState(prevState => ({
                ...prevState,
                errors: {
                    name: false,
                    email: false,
                    password: false,
                },
                isForgotPassword: false
            }))
        }
        else
            this.setState(prevState => ({
                ...prevState,
                errors: {
                    name: false,
                    email: false,
                    password: false,
                },
                isSignup: !this.state.isSignup
            }))
    }

    //sign-in & sign-up & forgotPassword 
    submit = (dispatch, e) => {
        const { isSignup, isForgotPassword, name, email, password } = this.state;
        let response;
        this.setState(prevState => ({
            ...prevState,
            errors: {
                ...prevState.errors,
                name: isSignup && (!name || validator.isEmpty(name) || !validator.isByteLength(name, { min: 3, max: 15 })),
                email: (!email || validator.isEmpty(email) || !validator.isEmail(email)),
                password: !isForgotPassword && (!password || validator.isEmpty(password) || !validator.isByteLength(password, { min: 6, max: 20 })),
            }
        }))

        if (!isSignup && isForgotPassword) {
            axios
                .post('http://localhost:3001/api/v1/forgotPassword', { email }, {
                    headers: { 'Authorization': "Bearer " + this.cookies("token") }
                })
                .then(result => {
                    let msgType = (result.data && result.data.errMsg) ? "errors" : "success"
                    if (result.data) {
                        this.setState(prevState => ({
                            ...prevState,
                            [msgType]: {
                                ...prevState[msgType],
                                general: `${result.data.message} Temporary Password: ${result.data.newPassword}`,
                            },
                            password: result.data.newPassword
                        }))
                        this.registerOrGoBack()
                    }
                }).catch((err) => {
                    console.log("err", err)
                });
            return;
        } else if (isSignup) {
            if (name && email && password) {
                this.setState({ disabledInput: true })
                axios.post('http://localhost:3001/api/v1/register', {
                    username: name,
                    email,
                    password
                })
                    .then(result => {
                        this.setState({ disabledInput: false })
                        this.setState({ name: "", password: '', disabledInput: false })
                        this.registerOrGoBack();
                    })
                    .catch((error) => {
                        response = error.response.data.message;
                        this.setState(prevState => ({
                            errors: {
                                ...prevState.errors,
                                general: response
                            }
                        }))
                        this.setState({ disabledInput: false })
                    });
            }
        } else if (email && password) {
            this.setState({ disabledInput: true })
            axios.post('http://localhost:3001/api/v1/login', {
                email,
                password
            })
                .then(result => {
                    response = result;
                    const { data } = result;
                    if (data.successMsg === "SUCCESS_LOGIN") {
                        axios
                            .get('http://localhost:3001/api/v1/trading/getAllExchange',
                                {
                                    headers: { 'Authorization': "Bearer " + data.token }
                                })
                            .then(() => {
                                data.email = this.state.email;
                                const cookies = new Cookies();
                                cookies.set('token', data.token, { path: '/' });
                                dispatch({ type: "SAVE_TOKEN", payload: data })
                                this.props.history.push('/home');

                            }).catch((err) => {

                            });
                        this.setState({ disabledInput: false })
                    } else if (data.errMsg === "NOT_FOUND_USER") {
                        this.setState({ email: '', password: '', disabledInput: false })
                    }
                })

                .catch(error => {
                    const { data } = error.response;
                    if (data.errMsg === "LOGIN_FAILED") {
                        this.setState(prevState => ({
                            ...prevState,
                            email: '',
                            password: '',
                            disabledInput: false,
                            errors: {
                                ...prevState.errors,
                                general: data.message
                            }
                        }))
                    }
                });
        }

    }

    valueUpdate = (e) => {
        const { isSignup, isForgotPassword, name, email, password } = this.state;
        this.setState({ [e.target.name]: e.target.value })
        if (isSignup)
            this.setState({ isValid: !(name && email && validator.isEmail(email) && password) })
        else if (!isForgotPassword)
            this.setState({ isValid: !(email && validator.isEmail(email) && password) })
        else
            this.setState({ isValid: !(email && validator.isEmail(email)) })

    }

    forgotPassword = () => {
        const { email } = this.state;
        this.setState({ isForgotPassword: true })
    }

    componentDidMount() {
        // if the user already signIn
        const cookies = new Cookies();
        if (cookies.get('token'))
            this.props.history.push('/home');
    }

    render() {
        const { isSignup, isForgotPassword, errors, name, email, password, isValid, disabledInput, success } = this.state;
        return (
            <Consumer>
                {
                    value => {
                        const { dispatch } = value;
                        return (
                            <div className="mt-5">
                                {success.general ? <AlertBox type="success" isDummyErr={false} error={success.general} /> : ""}
                                {errors.general ? <AlertBox isDummyErr={false} error={errors.general} /> : ""}
                                <img src="https://miro.medium.com/max/3200/1*a7N13eIm1mTJu9n0X5sE-Q.png" className="img-fluid bg-center" style={{ marginTop: "calc(-25%)" }} />
                                <Container className="mx-auto d-block shadow-lg p-3 mb-5 bg-white rounded" style={{ marginTop: "calc(-25%)", maxWidth: "calc(60vw)" }}>
                                    <Card style={{ border: "none" }}>
                                        <CardTitle className="text-center">
                                            <h2> {isSignup ? "Sign Up" : (!isForgotPassword ? "Sign In" : "Forgot Password")}</h2>
                                        </CardTitle>
                                        <Form className="form">
                                            {isSignup ? <Col>
                                                {/* <FormInput type="name" disabled={disabledInput} /> */}
                                                <FormGroup>
                                                    <Label htmlFor="name">FullName</Label>
                                                    <Input
                                                        type="name"
                                                        name="name"
                                                        id="name"
                                                        placeholder="Berat Tetik"
                                                        value={name}
                                                        disabled={disabledInput}
                                                        onChange={this.valueUpdate}
                                                    />
                                                    {errors.name ? <AlertBox type="danger" isDummyErr={false} error="You must enter your username" /> : ""}
                                                </FormGroup>
                                            </Col> : ""}
                                            <Col>
                                                <FormGroup>
                                                    <Label htmlFor="email" >E-mail</Label>
                                                    <Input
                                                        type="email"
                                                        name="email"
                                                        id="email"
                                                        placeholder="beratetik@gmail.com"
                                                        value={email}
                                                        disabled={disabledInput}
                                                        onChange={this.valueUpdate}
                                                    />
                                                    {errors.email ? <AlertBox type="danger" isDummyErr={false} error="You must enter E-mail" /> : ""}
                                                </FormGroup>
                                            </Col>
                                            <Col>
                                                {!isForgotPassword ?
                                                    <FormGroup>
                                                        <Label htmlFor="password">Password</Label>
                                                        <Input
                                                            type="password"
                                                            name="password"
                                                            id="password"
                                                            placeholder="********"
                                                            value={password}
                                                            disabled={disabledInput}
                                                            onChange={this.valueUpdate}
                                                        />
                                                        {errors.password ? <AlertBox type="danger" isDummyErr={false} error="You must enter Password!" /> : ""}
                                                    </FormGroup>
                                                    : ""}
                                            </Col>
                                            <Col className="text-center mb2">
                                                <Row>
                                                    <Col>
                                                        {!isSignup ? (!isForgotPassword ?
                                                            <Button disabled={disabledInput} className="" color="white" className="btn btn-outline-info" onClick={this.forgotPassword} > Forgot Password</Button> : "")
                                                            : ""}
                                                    </Col>
                                                    <Col>
                                                        <Button disabled={disabledInput} className="" color="white" className="btn btn-outline-primary" onClick={this.registerOrGoBack} > {isSignup ? "Back" : (isForgotPassword ? "Back" : "Register")}</Button>
                                                    </Col>
                                                    <Col>
                                                        <Button disabled={isValid} className="ml4" color="primary" onClick={this.submit.bind(this, dispatch)}>{isSignup ? "Register" : (isForgotPassword ? "Forgot Password" : "Sign In")}</Button>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Form>
                                    </Card>
                                </Container>
                            </div>

                        )
                    }}</Consumer>
        )
    }
}
