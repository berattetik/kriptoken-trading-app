import React, { useState } from 'react';
// import logo from './logo.svg';
// import './App.css';
import Navbar from './components/Navbar'
import Signup from './components/Signup'
import Home from './components/Home'
import { BrowserRouter as Router, Route } from "react-router-dom";

function App() {
  // const [isLoginOrRegister, setHomePage] = useState(true);
  return (
    <Router>
      <div className="App">
        {/* <Navbar isHomeScreen={isLoginOrRegister} /> */}
        <Navbar />
        <Route path="/" exact component={Signup} />
        <Route path="/home" component={Home} />
        {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      </div>
    </Router>
  );
}

export default App;
